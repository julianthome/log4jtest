
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.request.http.WebRequest;

public class Log4jTest {
    static Logger log = LogManager.getLogger(Log4jTest.class.getName());

    public void handle(WebRequest ex) {
        // example input 'X-TEST: ${jndi:ldap://127.0.0.1/org}'
        String test = ex.getHeader("X-TEST");
        log.info("Requested Api Version:{}", test);
    }
}